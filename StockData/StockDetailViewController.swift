//
//  StockDetailViewController.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import UIKit

class StockDetailViewController: UIViewController {
    
    var stock: Stock!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = self.stock.name
    }
}

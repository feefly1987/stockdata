//
//  Utils.swift
//  StockData
//
//  Created by EricChen on 15/12/5.
//  Copyright © 2015年 ERC. All rights reserved.
//

import Foundation

let dateFormatter: NSDateFormatter = {
    let formatter = NSDateFormatter()
    formatter.dateFormat = "yyyy-MM-dd"
    formatter.locale = NSLocale(localeIdentifier: "en_US")
    return formatter
}()

extension String {
    func dateStringByByAddingTimeInterval(interval: NSTimeInterval) -> String? {
        if let date = dateFormatter.dateFromString(self) {
            return dateFormatter.stringFromDate(date.dateByAddingTimeInterval(interval))
        }
        
        return nil
    }
    
    var floatValue: Float {
        return (self as NSString).floatValue
    }
    
    var doubleValue: Double {
        return (self as NSString).doubleValue
    }
    
    var integerValue: Int {
        return (self as NSString).integerValue
    }
    
    var stringValue: String {
        return self
    }
}

extension NSDate {
    var isWeekend: Bool {
        let calendar = NSCalendar.currentCalendar()
        let weekdayRange = calendar.maximumRangeOfUnit(.Weekday)
        let components = calendar.components(.Weekday, fromDate: self)
        let weekdayOfDate = components.weekday
        return weekdayOfDate == weekdayRange.location || weekdayOfDate == weekdayRange.length
    }
}
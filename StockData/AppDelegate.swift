//
//  AppDelegate.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import UIKit
import SVProgressHUD

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        SVProgressHUD.setDefaultStyle(SVProgressHUDStyle.Dark)
        return true
    }
}


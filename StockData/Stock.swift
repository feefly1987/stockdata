//
//  Stock.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import Foundation
import YYModel

private let reg = try! NSRegularExpression(pattern: "var hq_str_sh(.+?)=\"(.+?)\"", options: NSRegularExpressionOptions.CaseInsensitive)

class Stock: NSObject {
    var name: String = ""
    var code: String = ""
    var prices: [String: Float] = [:]
    
    override init() {super.init()}
    
    convenience init(name: String, code: String) {
        self.init()
        self.name = name
        self.code = code
    }
}

extension Stock {
    var dateCount: Int {
        return self.prices.keys.count;
    }
    
    var latestDate: NSDate? {
        return dateFormatter.dateFromString(self.prices.keys.sort().last ?? "")
    }
    
    var latestPrice: Float {
        return self.priceAtDate(self.latestDate ?? NSDate())
    }
    
    var latestBeforePrice: Float {
        return self.priceAtDate(self.latestDate?.dateByAddingTimeInterval(-24 * 60 * 60) ?? NSDate())
    }
    
    var latestGain: Float {
        let latestPrice = self.latestPrice
        let latestBeforePrice = self.latestBeforePrice
        
        if latestBeforePrice == 0 {
            return 0
        }
        
        return (latestPrice - latestBeforePrice) / latestBeforePrice
    }
    
    func gainAtDate(date: NSDate) -> Float {
        let price = self.priceAtDate(date)
        let beforePrice = self.priceAtDate(date.dateByAddingTimeInterval(-24 * 60 * 60))
        
        if beforePrice == 0 {
            return 0
        }
        
        return (price - beforePrice) / beforePrice
    }
    
    func priceAtDate(date: NSDate) -> Float {
        return self.priceAtDateString(dateFormatter.stringFromDate(date))
    }
    
    func priceAtDateString(date: String) -> Float {
        return self.prices[date] ?? 0
    }
    
    override var description: String {
        return "\(self.name)(\(self.code))，昨日价格：\(self.latestBeforePrice)，当前价格：\(self.latestPrice)，涨幅：\(String(format: "%.2f", self.latestGain * 100))%"
    }
}

extension Stock {
    class func modelPropertyBlacklist() -> [String] {
        return ["description"]
    }
}

extension Stock {
    func parseSinaInfo(info: String) {
        let matches = reg.matchesInString(info, options: .ReportCompletion, range: NSMakeRange(0, info.characters.count))
        if let match: NSTextCheckingResult = matches.first {
            let code = (info as NSString).substringWithRange(match.rangeAtIndex(1))
            let stockInfo = (info as NSString).substringWithRange(match.rangeAtIndex(2))
            
            if code != self.code {
                return
            }
            
            let infos = stockInfo.componentsSeparatedByString(",")
            let date = infos[infos.count - 3]
            self.prices[date] = infos[3].floatValue
            
            if let beforeDate = date.dateStringByByAddingTimeInterval(-24 * 60 * 60) {
                self.prices[beforeDate] = infos[2].floatValue
            }
        }
    }
}

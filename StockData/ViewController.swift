//
//  ViewController.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import UIKit
import SafariServices
import Alamofire
import SVProgressHUD
import SnapKit

class ViewController: UITableViewController {
    lazy var stocks: [Stock] = StockManager.manager.stocks

    @IBOutlet weak var refreshButtonItem: UIBarButtonItem!
    @IBOutlet weak var summaryButtonItem: UIBarButtonItem!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.configureView()
//        self.updateData()
    }
    
    private func configureView() {
    }
    
    private func updateData() {
        StockManager.manager.updateStocksInfo({[weak self] (progress, index, stock) -> Void in
            SVProgressHUD.showProgress(progress)
            self?.tableView.reloadRowsAtIndexPaths([NSIndexPath(forRow: index, inSection: 0)], withRowAnimation: .Automatic)
            }, completion: { () -> Void in
                SVProgressHUD.showSuccessWithStatus("更新完成")
        })
    }
    
    // MARK: UI Actions
    
    @IBAction func refreshButtonItemDidTap(sender: UIBarButtonItem) {
        self.updateData()
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "DETAIL_SEGUE" {
            let cell = sender as! UITableViewCell
            let index = self.tableView.indexPathForCell(cell)!.row
            let detailController = segue.destinationViewController as! StockDetailViewController
            detailController.stock = self.stocks[index]
        }
        
        super.prepareForSegue(segue, sender: sender)
    }
    
    // MARK: UITableViewDelegate
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let stock = self.stocks[indexPath.row]
        let URLString = "http://stockdata.stock.hexun.com/\(stock.code).shtml"
        let web = SFSafariViewController(URL: NSURL(string: URLString)!, entersReaderIfAvailable: true)
        self.navigationController?.pushViewController(web, animated: true)
    }
    
    // MARK: UITableViewDataSource
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stocks.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath)
        let stock = self.stocks[indexPath.row]
        cell.textLabel?.text = "\(stock)"
        return cell
    }
}


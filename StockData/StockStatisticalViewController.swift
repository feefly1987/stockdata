//
//  StockStatisticalViewController.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import UIKit

class StockStatisticalViewController: UITableViewController {
    @IBOutlet weak var titleButton: UIButton!
    
    var stocks:[Stock] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadStocksWithDay(3)
    }
    
    private func loadStocksWithDay(day: Int) {
        self.titleButton.setTitle("股票统计（\(day)天）", forState: .Normal)
        self.stocks = StockManager.manager.greaterStocksWithDay(day)
        self.tableView.reloadData()
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.stocks.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("CellIdentifier", forIndexPath: indexPath)
        let stock = self.stocks[indexPath.row]
        cell.textLabel?.text = "\(stock)"
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        tableView.deselectRowAtIndexPath(indexPath, animated: true)
    }
    
    // MARK: UI Actions
    
    @IBAction func titleButtonDidTap(titleButton: UIButton) {
        let alert = UIAlertController(title: "股票统计", message: nil, preferredStyle: .Alert)
        alert.addTextFieldWithConfigurationHandler { textField in
            textField.keyboardType = .NumberPad
        }
        alert.addAction(UIAlertAction(title: "取消", style: .Cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "确定", style: .Default, handler: { [weak self, weak alert] action in
            guard let dayString = alert?.textFields?.first?.text else {
                return
            }
            
            guard let day = Int(dayString) else {
                return
            }
            
            alert?.textFields?.first?.resignFirstResponder()
            self?.loadStocksWithDay(day)
        }))
        
        self.presentViewController(alert, animated: true, completion: nil)
    }
    
    @IBAction func saveButtonItemDidTap(sender: AnyObject) {
        let pasteboard = UIPasteboard.generalPasteboard()
        var info = ""
        for stock in stocks {
            info = "\(info)\(stock)\n"
        }
        
        pasteboard.string = info
        let alert = UIAlertController(title: "提示", message: "股票代码已复制到剪切板", preferredStyle: .Alert)
        alert.addAction(UIAlertAction(title: "确定", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alert, animated: true, completion: nil)
    }
}

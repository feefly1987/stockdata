//
//  StockManager.swift
//  StockData
//
//  Created by EricChen on 15/11/22.
//  Copyright © 2015年 ERC. All rights reserved.
//

import UIKit
import Alamofire
import YYCache
import YYModel

typealias StockUpdateProgress = (Float, Int, Stock) -> Void
typealias StockUpdateCompletion = dispatch_block_t
typealias StockFilter = (Stock, Stock) -> Bool

private let cache: YYCache = {
    return YYCache(name: "stock")
}()

class StockManager {
    static let manager = StockManager()
    
    lazy var stocks: [Stock] = {
        let cachedStocks = self.cachedStocks()
        if cachedStocks.count > 0 {
            return cachedStocks
        }
        
        let listPath = NSBundle.mainBundle().pathForResource("list", ofType: "txt")!
        let listInfo = try! NSString(contentsOfFile: listPath, encoding: NSUTF8StringEncoding)
        
        var stocks: [Stock] = []
        let allStocks = listInfo.componentsSeparatedByString("\n")
        for stock in allStocks {
            let stockInfo = stock.componentsSeparatedByString(", ")
            let name = stockInfo.first!
            let code = stockInfo.last!
            if name.characters.count > 0 && code.characters.count > 0 {
                let stock = Stock(name: name, code: code)
                stocks.append(stock)
            }
        }
        
        return stocks
    }()
    
    func updateStocksInfo(progress: StockUpdateProgress, completion: StockUpdateCompletion) {
        var finishCount = 0
        for (index, stock) in self.stocks.enumerate() {
            let url = "http://hq.sinajs.cn/list=sh\(stock.code)"
            
            Alamofire.request(.GET, url).responseString(completionHandler: {[unowned self] (response) -> Void in
                if let info = response.result.value {
                    stock.parseSinaInfo(info)
                }
                
                let currentProgress = Float(Double(++finishCount * 100 / self.stocks.count) / 100.0)
                progress(currentProgress, index, stock)
                
                if currentProgress >= 1 {
                    self.cacheStocks()
                    completion()
                }
            })
        }
    }
    
    private func cacheStocks() {
        let JSONArray = self.stocks.map { (stock) -> AnyObject in
            let object = stock.yy_modelToJSONObject()
            return object
        }
        
        cache.diskCache.setObject(JSONArray, forKey: "stocks")
    }
    
    private func cachedStocks() -> [Stock] {
        var stocks = [Stock]()
        
        if let JSONArray = cache.diskCache.objectForKey("stocks") as? [AnyObject] {
            JSONArray.forEach(){ object in
                if let stock = Stock.yy_modelWithJSON(object) {
                    stocks.append(stock)
                }
            }
        }
        
        return stocks
    }
    
    func filter(stocks: [Stock], _ filter: StockFilter) -> [Stock] {
        var filterStocks = [Stock]()
        
        for stock in stocks {
            if filter(stocks[0], stock) {
                filterStocks.append(stock)
            }
        }
        
        return filterStocks
    }
    
    func greaterStocksWithDay(day: Int) -> [Stock] {
        guard let root = self.stocks.first else {
            return []
        }
        
        guard let latestDate = root.latestDate else {
            return []
        }
        
        var dates = [NSDate]()
        var rootGains = [Float]()
        let maxDayCount = root.dateCount
        
        for index in 0..<min(day, maxDayCount) {
            let date = latestDate.dateByAddingTimeInterval(Double(-24 * 60 * 60 * index))
            if date.isWeekend { //周末没有数据，所以排除
                continue
            }
            
            dates.append(date)
            rootGains.append(root.gainAtDate(date))
        }
        
        return self.stocks.filter() { stock -> Bool in
            for (index, date) in dates.enumerate() {
                let stockGain = stock.gainAtDate(date)
                let rootGain = rootGains[index]
                
                if stockGain < rootGain {
                    return false
                }
            }
            
            return true
        }
    }
}
